set
	@group = 2,
	@freeLongerThan = 10;

set
	@freeLongerThan = COALESCE(@freeLongerThan, -1);

SELECT
	`uid`,
	`limit`,
	`free`
FROM
	`turns`
WHERE
	`group` = @group
	AND `free` >= @freeLongerThan;