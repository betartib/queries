/*
increase line limit
*/
set 
    @uid = 2,
    @amount = 60 * 60 * 24 * 2;
update `lines`
	set
		`limit` = `limit` + @amount,
		`free` = `free` + @amount
    where `uid` = @uid;