/*
make new line
*/
set
    @uid = 312312,
    @limit = 60 * 60 * 24 * 7,
    @group = 323432;

/* insert into lines */
insert into
    `lines` (`uid`, `limit`, `free`, `group`)
values
    (@uid, @limit, @limit, @group);