/*
setLineFilled
*/
set
    @uid = 2;

update
    `lines`
set
    `free` = 0
where
    `uid` = @uid;