set
	@length = 0,
	@line = 2,
	@uid = 2142343245345345,
	@priority = 1.0
set
	@priority = COALESCE(
		@priority,
		FLOOR(RAND() * 10000000) * 0.0000001 + UNIX_TIMESTAMP();

insert into
	`turns` (`uid`, `priority`, `line`, `length`)
values
	(@uid, @priority, @line, @length);

select
	@priority as `priority`;

