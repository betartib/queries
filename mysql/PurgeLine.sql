set
	@line_uid = 101;

delete from
	`lines`
where
	`uid` = @line_uid;

delete from
	`turns`
where
	`line` = @line_uid;