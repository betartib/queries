set
    @line = 123,
    @oldPriority = 34324.324,
    @newPriority = 34.43234;

UPDATE
    `turns`
SET
    `priority` = @newPriority
WHERE
    `line` = @line
    AND `priority` = @oldPriority;

