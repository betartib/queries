set
	@line = 2,
	@priorityAfterOf = 13.0000,
	@limit = 10;

set
	@priorityAfterOf = COALESCE(@priorityAfterOf, -1.0);

set
	SQL_SELECT_LIMIT = @limit;

SELECT
	`uid`,
	`priority`,
	`length`
FROM
	`turns`
WHERE
	`line` = @line
	AND `priority` >= @priorityAfterOf
ORDER BY
	`priority` ASC;

