set
	@line = 2,
	@priorityAfterOf = 1.0,
	@lengthLongerThan = 1.0;

set
	@priorityAfterOf = COALESCE(@priorityAfterOf, -1.0),
	@lengthLongerThan = COALESCE(@lengthLongerThan, -1);

SELECT
	COUNT(*) AS `count`,
	COALESCE(SUM(`length`), 0) AS `sum`
FROM
	`turns`
WHERE
	`line` = @line
	AND `priority` >= @priorityAfterOf
	AND `length` >= @lengthLongerThan;