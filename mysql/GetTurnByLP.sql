set
	@line = 2,
	@priority = 1.0;

select
	`uid`,
	`length`
from
	`turns`
where
	`line` = @line
	and `priority` = @priority;

